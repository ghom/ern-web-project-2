<div class="posts"><?php

$posts = posts();
if(count($posts) > 0){
    foreach($posts as $id => $post){
        extract($post);
        echo(
            '<div class="post">'.
                '<a href="'.$url.'">'.
                    '<div class="title">'. 
                        $title.
                    '</div>'.
                '</a>'.
                '<div class="content">'.
                    $content.
                '</div>'.
            '</div>'
        );
    }
}else{
    ?><div class="post">
        <div class="title">
            Aucun post à afficher.
        </div>
    </div><?php
}

?></div>