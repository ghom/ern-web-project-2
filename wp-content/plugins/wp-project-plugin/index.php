<?php

/*
Plugin Name: WP Project Plugin
Description: Bah c'est un plugin quoi...
Author: ERN
Version: 1.0.0
Author URL: https://discord.gg/3vC2XWK
*/

$options = [
    [
        'id' => 'pseudo',
        'name' => 'Identifiant'
    ],
    [
        'id' => 'life_response',
        'name' => 'Quelle est la réponse à la vie ?',
        'response' => '42'
    ],
    [
        'id' => 'why',
        'name' => 'Pourquoi ?',
        'response' => '42'
    ],
    [
        'id' => 'so',
        'name' => 'Et donc ?',
        'response' => '42'
    ]
];

register_activation_hook(__FILE__,function(){
    global $wpdb;
    $wpdb->query("CREATE TABLE IF NOT EXISTS
        {$wpdb->prefix}skill (
            id INT AUTO_INCREMENT PRIMARY KEY,
            pseudo VARCHAR(156) NOT NULL,
            score INT NOT NULL
        )
    ");
});
register_deactivation_hook(__FILE__,'desactivation');
register_uninstall_hook(__FILE__,'desactivation');
function desactivation(){
    global $wpdb;
    $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}skill");
}


add_action('admin_menu',function(){

    add_action('admin_init',function(){

        global $options;

        foreach($options as $option){
            register_setting('group_1',$option['id']);
        }
        register_setting('posts','nbr_post');

    });

    add_menu_page(
        'Ajouter des posts',
        'Ajouter des posts',
        'administrator',
        __FILE__,
        function(){

            global $wpdb;

            ?><div>
                <hr>
                <h1 class="title">Générer un nombre de post en Lorem</h1>
                <hr>
                <form action="" method="post"><?php 

                    settings_fields( 'posts' );
                    do_settings_sections( 'posts' );

                    ?><table>
                        <tr valign="top">
                        <th scope="row">Nombre de posts</th>
                        <td><input type="number" min="1" max="10000" name="nbr_post" value="1"/></td>
                        </tr>
                    </table><?php 
                
                    submit_button(); 

                    if(isset($_POST['nbr_post'])){
                        for($i=0; $i<intval($_POST['nbr_post']); $i++){
                            wp_insert_post([
                                'post_title'    => wp_strip_all_tags( 'Lorem Ipsum n°'.rand(1,200) ),
                                'post_content'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed nisl eu eros dignissim finibus. Cras nec nibh eget velit rhoncus eleifend. Aenean sed rhoncus sapien. Nulla hendrerit faucibus iaculis. Quisque sem erat, ultricies a quam non, bibendum fermentum mauris. Donec pharetra massa ac vulputate molestie. Sed in nisi magna. Nunc elementum, odio quis blandit rhoncus, felis metus ullamcorper lectus, sed fringilla massa nisi in purus. Phasellus sed ex at diam sodales rhoncus in et nisl.',
                                'post_status'   => 'publish',
                                'post_author'   => 1,
                                'post_category' => [rand(0,10) < 5 ? 5 : 6, rand(0,10) < 5 ? 1 : -1],
                            ]);
                        }
                    }
                ?></form>
            </div><?php
        }
    );

    add_menu_page(
        'Ajouter un illuminé',
        'Ajouter un illuminé',
        'administrator',
        __FILE__,
        function(){
            
            global $options;
            global $wpdb;

            ?><div>
                <hr>
                <h1 class="title">Ajouter un participant au questionnaire ultime de la vie.</h1>
                <h2>Tous les champs sont obligatoires.</h2>
                <hr>
                <form action="" method="post"><?php 

                    settings_fields( 'group_1' ); 
                    do_settings_sections( 'group_1' );

                    $filled = TRUE;
                    $score = 0;

                    ?><table><?php
                    
                    foreach($options as $option){
                            
                        $post = isset($_POST[$option['id']]) ? $_POST[$option['id']] : FALSE;

                        ?><tr valign="top">
                        <th scope="row"><?php echo $option['name'] ?></th>
                        <td><input type="text" name="<?php echo $option['id'] ?>" value="<?php 

                            if($post){
                                echo esc_attr($post);
                            }else{
                                echo esc_attr(get_option($option['id'])); 
                            }

                        ?>" /><?php

                            if($post !== FALSE){
                                if(trim($post) == ""){
                                    $filled = FALSE;
                                    echo ' Cette valeur est obligatoire.';
                                }else if($option['id'] == 'pseudo'){
                                    $pseudo = $post;
                                    $result = $wpdb->get_results("SELECT pseudo FROM {$wpdb->prefix}skill WHERE pseudo = '$pseudo'", OBJECT );
                                    $exist = isset($result) && count($result) > 0;
                                    if($exist){
                                        $filled = FALSE;
                                        echo ' ❌ Vous avez déjà entré un résultat avec cet identifiant.';
                                    }
                                }else{
                                    if(isset($option['response'])){
                                        if(strpos($_POST[$option['id']],$option['response']) !== FALSE){
                                            $score ++;
                                            echo ' ✅';
                                        }else{
                                            echo ' ❌';
                                        }
                                    }else{
                                        $filled = FALSE;
                                    }
                                }
                            }
                            ?></td>
                            </tr><?php
                        }
                    ?></table><?php 
                
                    submit_button(); 
                    if(isset($_POST['pseudo'])){
                        if($filled){
                            $query = "INSERT INTO {$wpdb->prefix}skill (
                                    pseudo, score
                                ) VALUES (
                                    '{$_POST['pseudo']}', $score
                                );
                            ";
                            $success = $wpdb->query($query);
                            if($success){
                                echo ' ✅ Votre réponse à été sauvegardée !';
                            }else{
                                echo ' ❌ Base de donnée inacessible...<br><pre>'.$query.'</pre>';
                            }
                        }else{
                            echo ' ❌ Veuillez remplir correctement le formulaire.';
                        }
                    }

                ?></form>
            </div><?php
        }
    );
});


add_action('widgets_init',function(){
    register_widget('Widget');
    // register_widget('Gario');
});

class Gario extends WP_Widget {

    function __construct(){
        parent::__construct(
            'gario',
            'Le mario des dieux',
            [
                'description' => 'Le mario des dieux'
            ]
        );
    }

    public function widget( $args, $instance ){

        echo $args['before_widget'];

        ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.7.3/p5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.7.3/addons/p5.dom.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.7.3/addons/p5.sound.min.js"></script>

        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/primary/box.js"></script>
        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/primary/hitbox.js"></script>
        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/primary/polygon.js"></script>
        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/primary/movebox.js"></script>

        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/elements/platform.js"></script>
        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/elements/wall.js"></script>
        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/elements/trap.js"></script>
        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/elements/checkpoint.js"></script>
        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/elements/cursor.js"></script>
        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/elements/enemy.js"></script>
        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/elements/player.js"></script>
        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/elements/level.js"></script>
        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/elements/button.js"></script>

        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/party.js"></script>
        <script src="<?php echo plugin_dir_url( __DIR__ ) ?>Gario/sketch.js"></script>
        <?php

        echo $args['after_widget'];
    }

    public function form( $instance ){}

    public function update( $new, $old ){
        return $old;
    }
}

class Widget extends WP_Widget {

    function __construct(){
        parent::__construct(
            'widget',
            'Les plus illuminés',
            [
                'description' => 'Petit widget pour le projet.',
                'customize_selective_refresh' => TRUE
            ]
        );
    }

    public function widget( $args, $instance ){
        global $wpdb;
        $limit = isset($instance['limit']) ? $instance['limit'] : 5;
        $query = "SELECT pseudo, score FROM {$wpdb->prefix}skill WHERE score > 0 ORDER BY score DESC LIMIT $limit";
        $result = $wpdb->get_results($query, OBJECT);

        echo $args['before_widget'];

        $title = apply_filters( 'widget_title', $instance['title'] );
        if(!empty($title)) echo $args['before_title'].$title.$args['after_title'].'<hr>';

        if($result){
            ?><ol><?php
                foreach($result as $i => $user){
                    ?><li><?php echo $i.'# | '.$user->score.' pts | '.$user->pseudo ?></li><?php
                }
            ?></ol><?php
        }else{
            ?><p>
                Aucun illuminé pour le moment.
            </p><?php
        }

        echo $args['after_widget'];
    }

    public function form( $instance ){
        $title = NULL;
        $limit = 5;
        if(isset($instance['limit'])){
            $limit = $instance['limit'];
        }
        if(isset($instance['title'])){
            $title = $instance['title'];
        }else{
            $title = "Top $limit des illuminés";
        }
        ?><label>
            Titre
            <input type="text" class="widefat" name="<?php echo $this->get_field_name('title') ?>" value="<?php echo esc_attr($title) ?>">
        </label>
        <label>
            Limite
            <input type="number" class="widefat" name="<?php echo $this->get_field_name('limit') ?>" value="<?php echo $limit ?>" min="1" max="10">
        </label><?php
    }

    public function update( $new, $old ){
        return [
            'title' => empty($new['title']) ? $old['title'] : strip_tags($new['title']),
            'limit' => $new['limit']
        ];
    }
}