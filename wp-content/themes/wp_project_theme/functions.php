<?php

//=======================================================================
//                                                                       
//  ##      ##  #####          #####   ##   ##  #####  ##   ##  #####  
//  ##      ##  ##  ##        ##   ##  ##   ##  ##     ##   ##  ##     
//  ##  ##  ##  #####         ##   ##  ##   ##  #####  ##   ##  #####  
//  ##  ##  ##  ##             #####   ##   ##  ##     ##   ##  ##     
//   ###  ###   ##                ##    #####   #####   #####   #####  
//                                                                       
//=======================================================================
 
add_action( 'wp_enqueue_scripts', function(){
	wp_enqueue_style( 'style', get_stylesheet_uri() );
});

//==============================================================================
//                                                                              
//  ##     ##    ###    ##   ##        ###    ###  #####  ##     ##  ##   ##  
//  ####   ##   ## ##   ##   ##        ## #  # ##  ##     ####   ##  ##   ##  
//  ##  ## ##  ##   ##  ##   ##        ##  ##  ##  #####  ##  ## ##  ##   ##  
//  ##    ###  #######   ## ##         ##      ##  ##     ##    ###  ##   ##  
//  ##     ##  ##   ##    ###          ##      ##  #####  ##     ##   #####   
//                                                                              
//==============================================================================

add_action('init',function(){
    register_nav_menu('primary','Primary Menu');
    register_nav_menu('secondary','Secondary Menu');
});

//===========================================================
//                                                           
//  ##      ##  ##  ####     ####    #####  ######   ####  
//  ##      ##  ##  ##  ##  ##       ##       ##    ##     
//  ##  ##  ##  ##  ##  ##  ##  ###  #####    ##     ###   
//  ##  ##  ##  ##  ##  ##  ##   ##  ##       ##       ##  
//   ###  ###   ##  ####     ####    #####    ##    ####   
//                                                           
//===========================================================

add_action('widgets_init',function(){
    register_sidebar([
        'name' => 'Widgets',
        'id' => 'widgets',
        'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
    ]);
});

//=========================================================================================================
//                                                                                                         
//  ###    ###  ##    ##        #####  ##   ##  ##     ##   ####  ######  ##   #####   ##     ##   ####  
//  ## #  # ##   ##  ##         ##     ##   ##  ####   ##  ##       ##    ##  ##   ##  ####   ##  ##     
//  ##  ##  ##    ####          #####  ##   ##  ##  ## ##  ##       ##    ##  ##   ##  ##  ## ##   ###   
//  ##      ##     ##           ##     ##   ##  ##    ###  ##       ##    ##  ##   ##  ##    ###     ##  
//  ##      ##     ##           ##      #####   ##     ##   ####    ##    ##   #####   ##     ##  ####   
//                                                                                                         
//=========================================================================================================

function template(string $include):void
{
    get_template_part('templates/'.$include);
}

function templates(array $includes):void
{
    foreach($includes as $include)
    {
        template($include);
    }
}

function posts():array
{
    $posts = array();
    while(have_posts()){
        the_post();
        $posts[get_the_ID()] = [
            'id' => get_the_ID(),
            'author' => get_the_author(),
            'title' => get_the_title(),
            'content' => get_the_content(),
            'url' => get_the_permalink(),
        ];
    }
    return $posts;
}

function page(array $includes):void 
{
    echo '<!DOCTYPE html>';
    echo '<html '; language_attributes(); echo '>';

        echo '<head>'.
            '<meta charset="'; bloginfo('charset'); echo '">'.
            '<title>'; bloginfo('name'); echo '</title>';
            wp_head();
        echo '</head>';

        echo '<body '; body_class(); echo '>';
            templates($includes);
        echo '</body>';

    echo '</html>';
}
