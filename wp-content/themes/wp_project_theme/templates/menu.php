<?php

wp_nav_menu([
    'theme_location' => 'primary', 
    'container_class' => 'primary-menu'
]);
wp_nav_menu([
    'theme_location' => 'secondary', 
    'container_class' => 'secondary-menu'
]);